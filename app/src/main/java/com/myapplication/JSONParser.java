package com.myapplication;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by egork on 27.12.2017.
 */

public class JSONParser {
    private static Response response;
    /**
     * Get Data From WEB
     *
     *
     * @return JSON Object
     */
    public static final String KEY_URL = "https://api.github.com/users?since=";
    public static final String KEY_ID = "id";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_AVATAR_URL = "avatar_url";
    public static final String KEY_HTML_URL = "html_url";
    public static final String KEY_TYPE = "type";


    public static List<JSONArray> getDataFromWeb(int size) {
        List<JSONArray> jsonArrayList = new ArrayList<JSONArray>();
        for(int i =0;i < size;i++) {
            try {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(KEY_URL + i + 00)
                        .build();
                Log.d("THREEAD", KEY_URL + i + "00");
                response = client.newCall(request).execute();
                jsonArrayList.add(new JSONArray(response.body().string()));
            } catch (@NonNull IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArrayList;
    }
}