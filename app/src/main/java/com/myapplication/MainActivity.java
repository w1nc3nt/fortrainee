package com.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ReposParser thread;
    Handler handler;

    List<JSONDataModel> jsonDataModelList = new ArrayList<JSONDataModel>();
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_frame);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        handler = new Handler(){
            public void handleMessage(Message msg){
                if(jsonDataModelList.size() > 1) {
                    progressBar.setVisibility(View.INVISIBLE);
                    bindList(jsonDataModelList);
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.button):
                if(isOnline(this)) {
                    progressBar.setVisibility(View.VISIBLE);
                    thread = new ReposParser(true, this);
                    thread.getThread().start();
                }else{
                    Toast.makeText(this,"Check your internet connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case(R.id.button2):
                removeAll();

        }
    }

    public boolean isOnline(Context context) { //Проверяем соединение с интернетом
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle d){
        super.onSaveInstanceState(d);
            if(jsonDataModelList.size() > 1){
                ArrayList<String> id = new ArrayList<String>();
                ArrayList<String> login = new ArrayList<String>();
                ArrayList<String> avatar_url = new ArrayList<String>();
                ArrayList<String> html_url = new ArrayList<String>();
                ArrayList<String> type = new ArrayList<String>();
                Parcelable[] parcelables = new Parcelable[jsonDataModelList.size()];
                int counter =0;
                for(JSONDataModel e : jsonDataModelList){
                    id.add(e.getId()); login.add(e.getLogin()); avatar_url.add(e.getAvatarUrl()); html_url.add(e.getHtmlUrl()); type.add(e.getType()); parcelables[counter] = e.getBitmapAvatar(); counter++;
                }
                d.putStringArrayList("id",id);
                d.putStringArrayList("login",login);
                d.putStringArrayList("avatar_url",avatar_url);
                d.putStringArrayList("html_url",html_url);
                d.putStringArrayList("type",type);
                d.putParcelableArray("bitmap", parcelables);
               d.putBoolean("isSaved",true);
            }else{d.putBoolean("isSaved",false);}
        }


    @Override
    protected void onRestoreInstanceState(Bundle restore){
        super.onRestoreInstanceState(restore);
        if(restore.getBoolean("isSaved")){
            Log.d("THREEAD","Restoring...");
           for(int i = 0;i < restore.getStringArrayList("id").size();i++){
                jsonDataModelList.add(new JSONDataModel(restore.getStringArrayList("id").get(i),restore.getStringArrayList("login").get(i),restore.getStringArrayList("avatar_url").get(i),
                        restore.getStringArrayList("html_url").get(i),restore.getStringArrayList("type").get(i),null,(Bitmap)restore.getParcelableArray("bitmap")[i]));
            }
            bindList(jsonDataModelList);
        }
    }

    void bindList(List<JSONDataModel> list){ //С помощью RecyclerView адаптера создаем CardView's и наполняем их данными из коллекции данных JSONDataModel
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewScroll);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(false);
        JSAdapter jsAdapter = new JSAdapter(list,this);
        recyclerView.setAdapter(jsAdapter);
    }

    void removeAll(){ //Очищаем активити
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewScroll);
        recyclerView.removeAllViewsInLayout();
        jsonDataModelList = new ArrayList<JSONDataModel>();
    }


    @Override
    protected void onResume(){
        super.onResume();
        if(thread != null) {
            thread.giveActivityState(true);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(thread != null) {
            thread.giveActivityState(false);
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(thread != null) {
            thread.giveActivityState(false);
        }
    }


}
