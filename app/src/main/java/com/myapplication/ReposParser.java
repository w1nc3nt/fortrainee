package com.myapplication;

import android.content.res.Resources;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class ReposParser implements Runnable {
    Thread thread;
    volatile Boolean activityState;
    volatile WeakReference<MainActivity> rewer;

    ReposParser(Boolean activityState,MainActivity m){
        rewer = new WeakReference<MainActivity>(m);
        thread = new Thread(this);
        this.activityState = activityState;
    }

    public Thread getThread(){
        return thread;
    }

    public void giveActivityState(Boolean activityState){ //Отслеживаем состояние основного UI потока
        this.activityState = activityState;
    }

    @Override
    public void run() {
        MainActivity mainact = rewer.get();
        List<JSONDataModel> jsonDataModelList = null;
        List<JSONArray> sobject = JSONParser.getDataFromWeb(2);
        if (sobject != null) {
            jsonDataModelList = getArrayJsonList(sobject);
        }
        while(true) { //Пытаемся передать коллекцию JSONDataModel в основной UI поток, до тех пор пока activityState не станет = true
            if (activityState) {
                mainact.jsonDataModelList = jsonDataModelList;
                mainact.handler.sendEmptyMessage(1);
                break;
            }
        }
    }

    private List<JSONDataModel> getArrayJsonList(List<JSONArray> jsonArray) { //Конвертируем данные из JSONArray в JSONDataModel
        List<JSONDataModel> jsonDataModelList = new ArrayList<>();
        for (JSONArray e : jsonArray) {
            for (int i = 0; i <= e.length(); i++) {
                Resources resources = null;
                if (activityState) {
                    MainActivity mainActivity = rewer.get();
                    resources = mainActivity.getResources();
                }
                try {
                    jsonDataModelList.add(new JSONDataModel(e.getJSONObject(i).getString(JSONParser.KEY_ID), e.getJSONObject(i).getString(JSONParser.KEY_LOGIN), e.getJSONObject(i).getString(JSONParser.KEY_AVATAR_URL),
                            e.getJSONObject(i).getString(JSONParser.KEY_HTML_URL), e.getJSONObject(i).getString(JSONParser.KEY_TYPE), resources, null));
                } catch (JSONException ei) {
                    ei.printStackTrace();
                }
            }
        }
        return jsonDataModelList;
    }

}
