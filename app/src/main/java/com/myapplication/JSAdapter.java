package com.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by egork on 01.01.2018.
 */

public class JSAdapter extends RecyclerView.Adapter<JSAdapter.JSONViewHolder> {

    public static class JSONViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView login;
        TextView id;
        TextView type;
        ImageView avatar;
        Button html;

        JSONViewHolder(View item){
            super(item);
            cv = (CardView) item.findViewById(R.id.cardVi);
            login = (TextView) item.findViewById(R.id.textLogin);
            avatar = (ImageView) item.findViewById(R.id.img_js);
            id = (TextView) item.findViewById(R.id.textId);
            type = (TextView) item.findViewById(R.id.textType);
            html = (Button) item.findViewById(R.id.buttonHtml);
        }
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(html_url));
            context.startActivity(intent);
        }
    };
    String html_url = "";
    Context context;

    List<JSONDataModel> jsonDataModelList;
    public JSAdapter(List<JSONDataModel> jsonDataModelList,Context context){
        this.jsonDataModelList = jsonDataModelList;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public JSONViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sda, viewGroup, false);
        JSONViewHolder jsonViewHolder = new JSONViewHolder(view);
        return jsonViewHolder;
    }

    @Override
    public void onBindViewHolder(JSONViewHolder jsonViewHolder, int i){
        jsonViewHolder.login.setText(jsonDataModelList.get(i).getLogin());
        jsonViewHolder.id.setText(jsonDataModelList.get(i).getId());
        jsonViewHolder.type.setText(jsonDataModelList.get(i).getType());
        jsonViewHolder.html.setText(jsonDataModelList.get(i).getHtmlUrl());
        jsonViewHolder.html.setOnClickListener(mOnClickListener);
        html_url = jsonDataModelList.get(i).getHtmlUrl();
        if(jsonDataModelList.get(i).getBitmapAvatar() !=null) {
            jsonViewHolder.avatar.setImageBitmap(jsonDataModelList.get(i).getBitmapAvatar());
        }
    }

    @Override
    public int getItemCount() {
        return jsonDataModelList.size();
    }

}
