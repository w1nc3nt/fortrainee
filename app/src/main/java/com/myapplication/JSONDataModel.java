package com.myapplication;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by egork on 01.01.2018.
 */

public class JSONDataModel {
    private String id;
    private String login;
    private String avatar_url;
    private String html_url;
    private String type;
    private Bitmap bitmap_avatar;

    JSONDataModel(){}

    public JSONDataModel(String id,String login,String avatar_url,String html_url,String type, Resources resources, Bitmap bitmap_avatar){
        this.id = id;
        this.login = login;
        this.avatar_url = avatar_url;
        this.html_url = html_url;
        this.type = type;
        if(resources !=null) {
            this.bitmap_avatar = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher);
        }
        if(bitmap_avatar !=null){
            this.bitmap_avatar = bitmap_avatar;
        }else if(avatar_url !=null){
            this.bitmap_avatar = downloadImage(avatar_url);
        }else {
            this.bitmap_avatar = null;
        }
    }

    public Bitmap downloadImage(String fileurl){
        URL myfileurl =null;
        Bitmap tmp = null;
        try
        {
            myfileurl= new URL(fileurl);
        }catch (MalformedURLException e)
        {
            Log.d("THREEAD", e.getLocalizedMessage());
        }
        try{
            HttpURLConnection conn= (HttpURLConnection)myfileurl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            int length = conn.getContentLength();
            int[] bitmapData =new int[length];
            byte[] bitmapData2 =new byte[length];
            InputStream is = conn.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            tmp = BitmapFactory.decodeStream(is,null,options);
        }catch (IOException e){
            Log.d("THREEAD", e.getLocalizedMessage());
        }
        return tmp;
    }

    public String getId(){return id;}
    public String getLogin(){return login;}
    public String getHtmlUrl(){return html_url;}
    public String getType(){return type;}
    public Bitmap getBitmapAvatar(){return bitmap_avatar;}
    public String getAvatarUrl(){return avatar_url;}
}
